# Android Studio Starter List app #

This repository holds a Skeleton applications created using the Android Studio IDE.
It's purpose is to speed up the initial development of a Android application by including many of the most commonly used Libraries/components in android apps today.

# What is currently included in this starter app? #

- Build using Android studio and Gradle for dependencies
- Targets Android api level 21 - Lollipop (V5.0) SDK apis, with a minimum api level of 16 - Android Jelly Bean(V4.1). 
- Includes the latest (at time of writing 15th June 2015) Android google support and compat libraries:'com.android.support:appcompat-v7:21.0.3' and 'com.android.support:support-v4:21.0.3'
- Includes the Picasso library, making download and display of images from sdcard/assets dir/remote urls simple
- Includes the example Navigation Drawer with three screens.
- Includes a example content provider backed by Sqlite db.
- A sample list screen is implemented, utilising a Cursor Loader that pulls list item data from the Content provider.
- The list items show example of how you can customise the data shown (via a binder) in a item instead of just pulling values from db.
- Example use of Picasso library to display images in the list screen list items.

### How do I get set up? ###

- Clone this git repository.
- Download and install the latest Android Studio IDE (its free).
- Make sure you have Android sdk 21 installed (run Android sdk manager from Android studio to install it if needed).
- In android studio, choose 'Import project' from the File menu, pointing it to the directory where you cloned the app.
- Android studio should automatically run Gradle to sync and install all required dependencies (specified in gradle.build).
- Change the class name to match your own. Do this by right clicking on the package named 'com.spottedparrot.android.starterlistapp' and then selecting 'refactor -> rename'.
- Build project and run in emulator!
- Profit from saving at least a days repetive effort ;-)

### License ###
Freeware.

This project is provided 'as is, without support or liability'. 
You can use the application any way you like. A note in your app giving credit to Spotted Parrot Ltd would be very much appreciated.
