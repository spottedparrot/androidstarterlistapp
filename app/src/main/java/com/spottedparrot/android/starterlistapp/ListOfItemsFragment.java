package com.spottedparrot.android.starterlistapp;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;


import com.spottedparrot.android.starterlistapp.db.DbModels.ItemColumns;
import com.spottedparrot.android.starterlistapp.db.DbProvider;
import com.spottedparrot.android.starterlistapp.dummy.DummyContent;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class ListOfItemsFragment extends ListFragment implements AbsListView.OnItemClickListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    public static final String TAG = "ListOfItemsFragment";

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    //private ListAdapter mAdapter;
    // The list adapter that pulls list items from the Sqlite DB
    private SimpleCursorAdapter mAdapter;

    private Context mContext;

    /*
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    */
    private static final String ARG_SECTION_NUMBER = "section_number";

    // TODO: Rename and change types of parameters
    /*
    private String mParam1;
    private String mParam2;
    */
    private int mSectionNumberParam;

    private OnFragmentInteractionListener mListener;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    // TODO: Rename and change types of parameters
    //public static ListOfItemsFragment newInstance(String param1, String param2) {
    public static ListOfItemsFragment newInstance(int sectionNumber) {
        ListOfItemsFragment fragment = new ListOfItemsFragment();
        Bundle args = new Bundle();

        // Enable these if you want to pass data into your fragment.
        /*
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        */

        args.putInt(ARG_SECTION_NUMBER, sectionNumber);

        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ListOfItemsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            // Enable these if you want to pass data into your fragment.
            /*
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            */

            mSectionNumberParam = getArguments().getInt(ARG_SECTION_NUMBER);

        }

        populatedListWithTestItems();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listofitems, container, false);

        // Set the adapter to the list built into this ListFragment
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        // Initialise the Android loader to populate list via DB
        setupLoader();

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;

            mContext = getActivity().getApplicationContext();

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onFragmentInteraction(DummyContent.ITEMS.get(position).id);
        }
    }

    private void populatedListWithTestItems() {

        // If we dont have any items in the list, add some:
        try {
            // Query db:
            // A "projection" defines the columns that will be returned for each row
            String[] mProjection =
                    {
                            ItemColumns._ID,    // Contract class constant for the _ID column name
                            ItemColumns.content,
                            ItemColumns.description,
                            ItemColumns.image_path
                    };

            // Defines a string to contain the selection clause
            //String mSelectionClause = LoadColumns._ID + "=?";
            String mSelectionClause = null;

            // Initializes an array to contain selection arguments
            //String[] mSelectionArgs = {"" + loadId};
            String[] mSelectionArgs = null;

            // Defines an object to contain the updated values
            /*
            ContentValues mUpdateValues = new ContentValues();
            mUpdateValues.put(ItemColumns.loa_fk_luls, ItemColumns.LOAD_STATE_VALUE_STARTED);
            mUpdateValues.put(ItemColumns.loa_start_time, formattedStartTime);
            */

            int mRowsUpdated = getActivity().getContentResolver().delete(
                    ItemColumns.CONTENT_URI,  // The content URI of the items table
                    mSelectionClause,
                    mSelectionArgs
            );

            // Some providers return null if an error occurs, others throw an exception
            if (mRowsUpdated == 0) {
                Log.e(TAG, "There are no items to delete. Thats ok.");
            }

            List<ContentValues> newValuesList = new ArrayList<ContentValues>();

            // Add some test items to the db:

            // Item 1:
            ContentValues newItemValues = new ContentValues();
            newItemValues.put(ItemColumns.content, "Bill");
            newItemValues.put(ItemColumns.description, "I'm a Geography student");
            newItemValues.put(ItemColumns.image_path, "file:///android_asset/1434040233_male3.png");

            newValuesList.add(newItemValues);

            // Item 2:
            newItemValues = new ContentValues();
            newItemValues.put(ItemColumns.content, "Fiona");
            newItemValues.put(ItemColumns.description, "Hi Mum!");
            newItemValues.put(ItemColumns.image_path, "file:///android_asset/1434040294_female1.png");

            newValuesList.add(newItemValues);

            // Item 4:
            newItemValues = new ContentValues();
            newItemValues.put(ItemColumns.content, "Bob");
            newItemValues.put(ItemColumns.description, "Hi Dad!");
            newItemValues.put(ItemColumns.image_path, "file:///android_asset/1434040302_malecostume.png");

            newValuesList.add(newItemValues);

            // Item 5:
            newItemValues = new ContentValues();
            newItemValues.put(ItemColumns.content, "Louise");
            newItemValues.put(ItemColumns.description, "I don't know what to say here.");
            newItemValues.put(ItemColumns.image_path, "file:///android_asset/1434040314_girl.png");

            newValuesList.add(newItemValues);

            // Item 6:
            newItemValues = new ContentValues();
            newItemValues.put(ItemColumns.content, "Jim");
            newItemValues.put(ItemColumns.description, "Hello, how can I help?");
            newItemValues.put(ItemColumns.image_path, "file:///android_asset/1434040320_supportmale.png");

            newValuesList.add(newItemValues);


            /*
            addItem(new DummyItem("6", "Jane Tech support", "Hello, do you have a ticket open?", "file:///android_asset/1434040325_supportfemale.png"));
            addItem(new DummyItem("7", "Manni Mannequin", "Help, I'm stuck in the shop ;-)", "file:///android_asset/1434040329_unknown.png"));
            */

            ContentValues[] bulkToInsert = new ContentValues[newValuesList.size()];
            newValuesList.toArray(bulkToInsert);

            getActivity().getContentResolver().bulkInsert(DbProvider.CONTENT_URI_ITEM, bulkToInsert);

        }
        catch (Exception e) {
            Log.e(TAG, "ERROR writing/deleting db items. Exception: " + e.getMessage());
        }

    }

    private void setupLoader() {

        // Pulling from DB using the Cursor adapter:

        // Fields from the database (projection) that we want to use in the listview item
        String[] from = new String[] {
                ItemColumns.image_path,
                ItemColumns.content,
                ItemColumns.description
        };

        // Which Fields in the listview items UI to put the db values into:
        int[] to = new int[] {
                R.id.image,
                R.id.content,
                R.id.description
        };

        getLoaderManager().initLoader(0, null, this);

        mAdapter = new SimpleCursorAdapter(getActivity().getApplicationContext(), R.layout.item_row, null, from,
                to, 0);

        // We set the view binder for the adapter to our own CustomViewBinder.
        // The code for the custom view binder is below.
        mAdapter.setViewBinder(new CustomViewBinder());

        setListAdapter(mAdapter);

        // Alternative ArrayList adapter (only enable one of these):
        /*
        mAdapter = new ArrayAdapter<DummyContent.DummyItem>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, DummyContent.ITEMS);

        setListAdapter(mAdapter);
        */

    }

    // This allows us to show a custom drawable and text in the list for Load_state instead of just showing db value.
    private class CustomViewBinder implements SimpleCursorAdapter.ViewBinder {

        @Override
        public boolean setViewValue(View view, Cursor cursor, int columnIndex) {

            // In most listview fields, you just want the value pasted into the ui.
            // But for the imageview, we want picasso to download the image and turn it
            // into a image (that gets shown)
            //
            // You can also use this method to reformat data from the db.
            // E.g. sqlite dates could be reformatted here.

            // Is the column the imagePath?
            if (columnIndex == cursor.getColumnIndex(ItemColumns.image_path)) {

                // If the column is the 'load state' then we use custom view.
                String image_path_value = cursor.getString(columnIndex);

                // Get picasso to get the image (whether it be local in assets or remote http:
                ImageView imageView = (ImageView) view;
                if (imageView == null) {
                    imageView = new ImageView(mContext);
                }

                Picasso.with(mContext)
                        .load(image_path_value)
                        .into(imageView);

                return true;
            }

            // For others, we simply return false so that the default binding
            // happens.
            return false;
        }

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String[] projection = {
                ItemColumns._ID,
                ItemColumns.content,
                ItemColumns.description,
                ItemColumns.image_path
        };

        String selection = null;

        //String[] selectionArgs = null; // we hardcode the selection values in selection string.

        // If you want to show different sets of data (maybe you have a spinner to filter). Then you can
        // enable multiple loaders. The switch on the loaders id like below):
        /*
        switch (id) {

            case FILTER_ONLY_FIELD1_ITEMS_POSITION:
                selection = ItemColumns.field1 + " = 1 ";
                break;

            case FILTER_NONE_POSITION:
                selection = null; // Show all items in db
                break;

        }
        */

        CursorLoader cursorLoader = new CursorLoader(getActivity().getApplicationContext(),
                DbProvider.CONTENT_URI_ITEM, projection, selection, null, null);

        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        // Useful for debug: As this method gets called on start or when loader has finised loading
        if (data != null) {
            Log.i(TAG, "onLoadFinished: data row count: " + data.getCount());
        }
        else {
            Log.i(TAG, "onLoadFinished: cursor is empty (null) - which can be normal.");
        }

        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // data is not available anymore, delete reference
        mAdapter.swapCursor(null);
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String id);
    }

}
