package com.spottedparrot.android.starterlistapp.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 *
 * This content is backed by a ArrayList of DummyItems (with a number id and a string name).
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static List<DummyItem> ITEMS = new ArrayList<DummyItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

    // If you want to start with an empty list, disable the following static block.
    // E.g. Maybe you want to get items via http rest api. Then you can call addItem()
    // in your http pull.
    static {
        // Add some sample items.
        // Note: the icons are put of the user-profiles iconset and are free to use for
        // personal and commercial use.
        // They can be seen here: https://www.iconfinder.com/iconsets/user-pictures

        addItem(new DummyItem("1", "Bill", "I'm a Geography student.", "file:///android_asset/1434040233_male3.png"));
        addItem(new DummyItem("2", "Fiona", "I'm a expert in String theory.", "file:///android_asset/1434040294_female1.png"));
        addItem(new DummyItem("3", "Bob", "Hi Mum!", "file:///android_asset/1434040302_malecostume.png"));
        addItem(new DummyItem("4", "Louise", "I don't know what to say here.", "file:///android_asset/1434040314_girl.png"));
        addItem(new DummyItem("5", "Jim Tech support", "Hello, how can I help?", "file:///android_asset/1434040320_supportmale.png"));
        addItem(new DummyItem("6", "Jane Tech support", "Hello, do you have a ticket open?", "file:///android_asset/1434040325_supportfemale.png"));
        addItem(new DummyItem("7", "Manni Mannequin", "Help, I'm stuck in the shop ;-)", "file:///android_asset/1434040329_unknown.png"));
    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    /**
     * A dummy item representing a piece of content and an associated image
     */
    public static class DummyItem {
        public String id;
        public String content;
        public String description;

        /**
         * imagePath: As we use the Picasso image caching library, the values of images can be:
         *
         * 1. Locally stored image in assets dir:
         *    file:///android_asset/imagename.png
         *
         * 2. Remote image from web:
         *    http://www.example.com/imagename.png
         *
         */
        public String imagePath;

        public DummyItem(String id, String content, String description, String imagePath) {
            this.id = id;
            this.content = content;
            this.description = description;
            this.imagePath = imagePath;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
