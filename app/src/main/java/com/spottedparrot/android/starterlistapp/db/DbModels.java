package com.spottedparrot.android.starterlistapp.db;

import android.net.Uri;
import android.provider.BaseColumns;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Convenience definitions for this apps Provider
 */
public final class DbModels {

    public static final String AUTHORITY = "com.spottedparrot.android.starterlistapp.provider.DbModels";

    // This class cannot be instantiated
    private DbModels() {}

    /**
     * Items table
     */
    public static final class ItemColumns implements BaseColumns {
        // This class cannot be instantiated
        private ItemColumns() {}

        /**
         * The content:// style URL for this table
         */
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/items");

        /**
         * The MIME type of {@link #CONTENT_URI} providing a directory of items.
         */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.google.item";

        /**
         * The MIME type of a {@link #CONTENT_URI} sub-directory of a single note.
         */
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.google.item";

        /**
         * The default sort order for this table
         */
        public static final String DEFAULT_SORT_ORDER = "_id DESC";

        /**
         * The items context string
         * <P>Type: STRING</P>
         */
        public static final String content = "content";

        /**
         * The items description string
         * <P>Type: STRING</P>
         */
        public static final String description = "description";

        /**
         * The images path
         * <P>Type: STRING</P>
         */
        public static final String image_path = "image_path";

    }

}
